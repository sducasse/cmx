This code is the implementation of a small environment for teaching programming to novices. 
A book is under edition. 

Early version of the book and more information are available 
at: http://kilana.unibe.ch:9090/caroandbot/

Contact ducasse@iam.unibe.ch if you want to give feedback, ask question


Version 40
==========
	- Totally managed via MC
	- new handles
	- all kinds of fixes
	- Miner not loaded

Version 34
==========
	- start to use 3.7
	
	- to do 
		- take latest version of shout
		- remove anyBot because it blocks
		- add animate button
		- remove fork in paragraphEditor
			since it blocks with aniBot
			and messes up with shout
	- painting


Version 28
==========
	- start to use MW 3.6
	- clean the files to be able to migrate to 3.7

Known Problems:
---------------
	- issues with filelist auto selection due to MW
	- the issues with anyButtonPressed see below

To do:
------ 
	- Turtle new (clearImage, loadImage, storeImage)
	- fine tune word completion
	- fix workspace variable declaration
	- add Turtle preferences in Preferences
	- do it and do it all in the menu of the workspace (shortcut also)
	- clean issues with AniTurtle and Turtle/fork/animated/
anyButtonPressed

Notes: 
------
One solution to the problem is to only use AniTurtle.
(Rename Turtle -> StaticTurtle)

The problem with AniTurtle is that 
	| caro |	caro := AniTurtle new.	caro doUntilButtonPressed: [caro go: 10 ; turn: 30]

endlessly loops if this is not forked

Then say that people should only use the Turtle workspace

Problems is that I would have to change while everywhere because
I cannot have [] while: [] but self doUntil: []


| caro |caro := Turtle new.caro wandHandAnyButtonPressed

works (we see the turtle animated)
 [| caro |caro := Turtle new.caro wandHandAnyButtonPressed ] fork

raise debugger...includes:...      normal => wrong solution

| caro |caro := Turtle new.caro wandSensorAnyButtonPressed

works but we do not see the animation


[| caro |caro := Turtle new.caro wandSensorAnyButtonPressed] fork
Works animation and stops

| caro |caro := AniTurtle new.caro wandSensorAnyButtonPressed.
works 
[| caro |caro := AniTurtle new.caro wandSensorAnyButtonPressed.]fork
works but may raise debugger
